﻿using System.ComponentModel.DataAnnotations;

namespace KIEMTRA.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        public int TransactionalID { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        public Transaction? Transaction { get; set; }
    }
}
