﻿namespace KIEMTRA.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public string Name { get; set; }    
        public Employee? Employee { get; set; }
        public Customer? Customer { get; set; }
    }
}
