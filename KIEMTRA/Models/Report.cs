﻿namespace KIEMTRA.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
        public int AccountID { get; set; }
        public int LogID { get; set; }
        public int EmployeeID { get; set; }
        public Transaction? Transaction { get; set; }
        public Log? Log { get; set; }
        public Account? Account { get; set; }
    }
}
