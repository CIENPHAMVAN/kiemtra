﻿using System.ComponentModel.DataAnnotations;

namespace KIEMTRA.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public decimal Balance { get; set; }

        public int CustomerID { get; set; }
        public virtual Customer? Customer { get; set; }
    }
}
